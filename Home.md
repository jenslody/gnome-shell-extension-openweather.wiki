![Screenshot](https://gitlab.com/jenslody/gnome-shell-extension-openweather/raw/master/data/Screenshot.jpg)

*gnome-shell-extension-openweather* is a simple extension for displaying weather conditions and forecasts in GNOME Shell, featuring support for multiple locations, no need for WOEID, a symmetrical layout and a settings panel through *gnome-shell-extension-prefs*.

The weather data is fetched from [OpenWeatherMap](https://openweathermap.org/) (including forecasts for up to ten days) or [forecast.io](https://forecast.io) (including forecasts for up to eight days).

#### Note: since version 29 this extensions uses coordinates to store the locations and makes the names editable to support multiple weather-providers!
#### If you update from versions prior to 29 to 29 or greater (with forecast.io - support) you have to recreate your locations.

* [Installation](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Installation)
* [Configuration](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Configuration)
* [Translate](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Translate)