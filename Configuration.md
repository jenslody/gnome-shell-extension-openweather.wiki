# Configuration

Launch *gnome-shell-extension-prefs* (reachable also through the *OpenWeather Settings* button on the extension popup, the *gnome-tweak-tools*, [Installed extensions](https://extensions.gnome.org/local/) or the [OpenWeather](https://extensions.gnome.org/extension/750/openweather/) site on [https://extensions.gnome.org/](https://extensions.gnome.org/) ) and select *OpenWeather* from the drop-down menu to edit the configuration.

![Screenshot](https://gitlab.com/jenslody/gnome-shell-extension-openweather/raw/master/data/weather-settings.gif)

You can also use *dconf-editor* or *gsettings* to configure the extension through the command line.

On [OpenWeatherMap](https://openweathermap.org/) you can either use the extensions default-key or register to get a [personal API key](http://openweathermap.org/appid). This key has to be added in the preferences dialog. Don't forget to switch the a appropriate switch in the dialog to "off" in this case.

To use [Forecast.io](https://forecast.io) you also need to [register](https://developer.forecast.io/register) and get an API key. With this key you can make 1000 requests per day for free. This should be enough for this extension in any normal use case. Do not add billing information, otherwise you might have to pay for the weather-data if something went wrong !

* [Home](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis)
* [Installation](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Installation)
* [Translate](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Translate)