# Installation

After the installation, restart GNOME Shell (`Alt`+`F2`, `r`, `Enter`) and enable the extension through *gnome-tweak-tool*.

## Through extensions.gnome.org (Local installation)

Go on the [OpenWeather](https://extensions.gnome.org/extension/750/openweather/) extension page on extensions.gnome.org, click on the switch ("OFF" => "ON"), click on the install button. That's it !

## Through a package manager

#### Note: you need the root password for all these installation modes, if you do not have root-access, and the needed build-dependencies are installed, use the generic install.

### [Debian](http://packages.debian.org/source/unstable/gnome-shell-extension-weather)

Debian uses the (former master now ) yahoo-branch !

My fork of the extension is currently only available for unstable/sid.

Install the package through APT (or use your favourite package-manager, e.g. synaptic):

	sudo apt-get install gnome-shell-extension-weather


### [Fedora](https://fedoraproject.org/)

You can install the extension from the official Fedora repos.

### [Arch Linux (AUR)](https://aur.archlinux.org/packages/gnome-shell-extension-openweather-git/)

	sudo yaourt -S gnome-shell-extension-openweather-git


## Generic (Local installation)

Make sure you have the following dependencies installed:
* *dconf*,
* *gettext*,
* *pkg-config*,
* *git*,
* *glib2 (and development packages)*,
* *zip*,
* *gnome-common*,
* *autoconf*,
* *automake*,
* *intltool*.
* *gnome-tweak-tool*.

Run the following commands:

	cd ~ && git clone git://gitlab.com/jenslody/gnome-shell-extension-openweather.git
	cd ~/gnome-shell-extension-openweather
	./autogen.sh && make local-install

* [Home](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis)
* [Configuration](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Configuration)
* [Translate](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Translate)