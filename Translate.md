It's easy to translate the extension in your language !

Translation of the weather-conditions is not used with [forecast.io](https://forecast.io), they provide already translated data (see: the lang-parameter in the [The Dark Sky Forecast API](https://developer.forecast.io/docs/v2) ). If different languages are desired, please consider contributing to their [API translation module](https://github.com/darkskyapp/forecast-io-translations) on Github.

## Requirement
* A GitLab account.
* `git`.
* `seed`.
* `poedit`.
* Fork the project.

## Clone your fork & go into po directory

    cd ~ && git clone git@gitlab.com:YOUR_USERNAME/gnome-shell-extension-openweather.git
    cd ~/gnome-shell-extension-weather/po

## Update the po directory

    seed update.js

## Edit existing languages
* Open the locale file for your language (each locale are named with [ISO 639](http://en.wikipedia.org/wiki/ISO_639) standard. Example for french, fr.po) with poEdit.
* Translate the file and save.
* Don't forget to remove the "#, fuzzy" lines, if the updated translation is correct, otherwise the translation will not be visible.
* Update the po directory.
* Send changes to your fork:

        git add YOUR_LANGUAGE_IN_ISO_639.po
        git commit -m 'Update YOUR_LANGUAGE translation'
        git push

## New language
* Open a new poEdit project using gnome-shell-extension-weather.pot and name it YOUR_LANGUAGE_IN_ISO_639.po
* Translate and save.
* Update the po directory.
* Send changes to your fork:

        git add YOUR_LANGUAGE_IN_ISO_639.po
        git commit -m 'Add YOUR_LANGUAGE translation'
        git push

## Now ![Pull request](http://www.xeked.com/images/pull_request.png) !

* [Home](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis)
* [Configuration](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Configuration)
* [Installation](https://gitlab.com/jenslody/gnome-shell-extension-openweather/wikis/Installation)